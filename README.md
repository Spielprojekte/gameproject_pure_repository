# Project Title: pure

Arbeitstitel des Spiels ist „pure“, welcher auf die angestrebte Reinheit der Seele anstrebt, die notwendig ist, um die Hölle zu verlassen. Weitere Möglichkeiten wären „Purgatory/Fegefeuer“ oder „Purification/ Reinigung“. Da das Rätselsystem sich vordergründig um die „Sieben Todsünden“ drehen wird, wäre auch eine Namensgebung in diesem Bereich denkbar, jedoch muss abgewogen werden, ob hierdurch zu viel von narrativen Aspekten vorgegriffen wird.
Das Spiel soll im Genre-Bereich des „Metroidvania“ liegen. Ein herausstechendes Merkmal ist hierbei, die Vernetzung einzelner Level über den Ausbau der Charakterfähigkeiten, etwa dass durch eine neue Fähigkeit Rätsel gelöst oder Hindernisse aus dem Weg geräumt werden, sodass neue Teile bekannter Bereiche freigeschaltet werden. Kernaspekte des Spiels sind demnach die miteinander vernetzte Struktur der Gebiete und Bereiche, welche sich ebenso auf die Rätselstruktur auswirkt, sowie die Entwicklung der Charakterfähigkeiten, durch Aktionsquantität oder Hilfsmittel.

## Getting Started

The project itself is found under the **pure** folder which itself is a Unity 2017 project and can be opened through the editor.
Under the **documentation** folder you can find concept-files and artwork as well as the game documentation and other important documents.
The **build** folder holds a tested Windows-build and a untested MacOS-build.
Some materials for conventions or pitches can be found in the **pitch-material** folder.

## Built With

Unity 2017.2.0f3

## Versioning

Version 0.5.6

## Authors

Svenja Süttenbach, Bayreuth

## License

This project is a student project at the University Bayreuth in Germany

## Acknowledgments


