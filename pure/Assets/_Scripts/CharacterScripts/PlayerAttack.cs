﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour {

	private PlayerStats stats;
	private Animator anim;
	private TriggerManager attackTrigger;
	private float attackTimer = 0;
	private float attackDamage = 0;

	private Transform eventManager;

	void Start ( ) {
		stats = GetComponent<PlayerStats> ( );
		anim = GetComponent<Animator> ( );
		attackTrigger = transform.Find ( "TriggerManager" ).GetComponent<TriggerManager> ( );
	}

	void Update ( ) {
		if ( stats.isAttacking ) {
			if ( attackTimer > 0 ) attackTimer -= Time.deltaTime;
			else {
				stats.isAttacking = false;
				attackTrigger.setEnabledOfAll ( false );
			}
		}
	}

	//checking if an enemy was in the hit-triggers
	void OnTriggerEnter2D ( Collider2D other ) {
		if ( other.tag == "Enemy" ) {
			other.gameObject.SendMessageUpwards ( "TakeDamage", attackDamage );
			attackDamage = 0;
		}
	}

	//striking
	public void Strike ( ) {
		//checking if motioned down or just facing sideways
		if ( Input.GetAxis ( "Vertical" ) < .0f ) {
			anim.SetTrigger ( "strikeDown" );
			attackTrigger.setEnabledOfColliderForAttack ( "strikeDown", true );
			EffectManager.Instance ( ).showEffectAt ( "strikeDown", transform.Find ( "GroundCheck" ).position );
			CameraShakeController.Instance ( ).shakeCamera ( 0.2f, 1f, 0.2f );
			attackDamage = stats.strikeDownDamage;
		} else {
			anim.SetTrigger ( "strike" );
			attackTrigger.setEnabledOfColliderForAttack ( "strike", true );
			attackDamage = stats.strikeStraightDamage;
		}        
		//forcing pause between attacks
		stats.isAttacking = true;
		attackTimer = stats.attackCooldown;
	}

	public void Shout ( ) {
		anim.SetTrigger ( "shout" );
		stats.isAttacking = true;
		attackTimer = stats.attackCooldown;
		attackTrigger.setEnabledOfColliderForAttack ( "shout", true );
		EffectManager.Instance ( ).showEffectAt ( "shout", transform.Find ( "torso" ).Find ( "head" ).position );
		AudioManager.Instance ( ).PlaySound ( "shout" );
		CameraShakeController.Instance ( ).shakeCamera ( 1f, 0.2f, 0.2f );
		attackDamage = stats.shoutDamage;
	}

	public void HeadBump ( ) {
		attackTrigger.setEnabledOfColliderForAttack ( "strike", true );
		stats.isAttacking = true;
		float xJumpForward = stats.smashForce * 10;
		if ( !stats.isFacingRight ) {
			xJumpForward *= -1;
		}

		transform.GetComponent<Rigidbody2D> ( ).AddForce ( new Vector2 ( xJumpForward, 0f ) );
		EffectManager.Instance ( ).showEffectAt ( "swoosh", transform.Find ( "torso" ).position );

		attackTimer = stats.attackCooldown;
		anim.Play ( "headbump" );
		attackDamage = stats.smashDamage;
	}



}
