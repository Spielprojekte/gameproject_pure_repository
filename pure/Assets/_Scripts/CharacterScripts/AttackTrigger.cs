﻿using System.Collections;
using UnityEngine;

public class AttackTrigger : MonoBehaviour {

    public string attackType;
    private TriggerManager trigMan;

    void Awake ( ) {
        trigMan = transform.parent.GetComponent<TriggerManager> ( );
    }

    void OnTriggerEnter2D ( Collider2D collider ) {        
        trigMan.TriggerOfChild2D ( collider, attackType );
    }

}
