﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulMovement : MonoBehaviour {

    public float maxSpeed;
    public float turnSpeed;

    private Vector2 speed;
    private bool isFacingRight = false;

    private Animator anim;
    private Rigidbody2D rb2d;


    void Start ( ) {
        anim = transform.GetComponent<Animator> ( );
        rb2d = transform.GetComponent<Rigidbody2D> ( );

    }

    void FixedUpdate ( ) {
        // get input
        speed = new Vector2 ( Input.GetAxis ( "Horizontal" ), Input.GetAxis ( "Vertical" ) );
        // look in the moving direction
        computeRotation ( );
        // move
        rb2d.velocity = new Vector2 ( speed.x * maxSpeed, speed.y * maxSpeed );
        // set animator
        anim.SetFloat ( "Speed", Mathf.Abs ( speed.x ) + Mathf.Abs ( speed.y ) );
    }

    /*
     * this method should compute the degree the character should be rotated on the z axis 
     * depending on the input of the player
     * adding to that the sprite should be flipped if the horizontal-axis is negative
     * important notice: set the physics of the rigidbody to kinematic or else the physics will mess with the rotation
     */
    private void computeRotation ( ) {
        
        // flipping the character
        if ( ( speed.x <= 0 && !isFacingRight ) || ( speed.x > 0 && isFacingRight ) ) {
            transform.localScale = new Vector3 ( transform.localScale.x * -1, transform.localScale.y, 1 );
            isFacingRight = !isFacingRight;
        }

        float angle = 0;
        // checking for input
        if ( speed.x != 0 || speed.y != 0 ) {
            // computing the angle between standard facing-direction and input-direction            
            if ( speed.x >= 0 ) angle = Vector2.Angle ( speed, Vector2.right );
            else if ( speed.x < 0 ) angle = Vector2.Angle ( speed, Vector2.left );
                   
            // adjusting the y-Input-Axis depending on the facing direction
            if ( !isFacingRight && speed.y < 0 ) angle *= -1;
            else if ( isFacingRight && speed.y > 0 ) angle *= -1;
        }

        if ( angle == 0 ) {
            // resetting the rotation and all external forces to 0
            transform.rotation = Quaternion.identity;
            rb2d.velocity = Vector3.zero;
            rb2d.angularVelocity = 0;
        } else {
            // apply the new rotation
            Quaternion target = Quaternion.Euler ( 0, 0, angle );
            transform.rotation = Quaternion.Slerp ( transform.rotation, target, Time.deltaTime * turnSpeed );
        }
    }
}
