﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerManager : MonoBehaviour {

    [SerializeField]
    private Collider2D [] colliders;

    void Awake ( ) {
        colliders = transform.GetComponentsInChildren<Collider2D> ( );
        setEnabledOfAll ( false );
    }

    public void setEnabledOfAll ( bool enable ) {
        foreach ( Collider2D coll in colliders ) {
            coll.enabled = enable;
        }
    }

    public void setEnabledOfColliderForAttack ( string attackType, bool enable ) {
        transform.Find ( attackType + "Collider" ).GetComponent<Collider2D> ( ).enabled = enable;

    }

    public void TriggerOfChild2D ( Collider2D collider, string attackType ) {
        switch ( attackType ) {
        case "strike":
            strikeStraight ( collider );
            break;
        case "strikeDown":
            strikeStraight ( collider );
            break;
        case "shout": 
            shout ( collider );
            break;
        default:
            Debug.Log ( "Collider detected without specified attackType: " + attackType );
            break;
        }
    }

    private void strikeStraight ( Collider2D collider ) {
        switch ( collider.gameObject.tag ) {
        case "Destructable":
            collider.SendMessageUpwards ( "Break" );
            break;
        default :
            break;
        }
    }

    private void strikeDown ( Collider2D collider ) {

    }

    private void shout ( Collider2D collider ) {

    }
}
