﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonMovement : MonoBehaviour {

	private Rigidbody2D rb2d;
	private Animator animator;
	private PlayerStats stats;
	private PlayerAttack attackScript;

	// temporary storage for interactable objects
	private GameObject interactableObject;

	// variables for groundcheck
	public LayerMask groundLayerMask;
	private Transform groundCheck;
	private const float groundCheckRadius = .2f;

	public float currentJumpForce;
	private float jumpStartHeight;
	private float bodyColliderHeight;


	void Awake ( ) {
		// Setting up references.
		stats = GetComponent<PlayerStats> ( );
		animator = GetComponent<Animator> ( );
		rb2d = GetComponent<Rigidbody2D> ( );
		attackScript = GetComponent<PlayerAttack> ( );
		groundCheck = transform.Find ( "GroundCheck" );
		rb2d.mass = stats.weight / 100;
		currentJumpForce = stats.maxJumpForce;
		bodyColliderHeight = GetComponent<CapsuleCollider2D> ( ).size.y;

	}

	/*
     * checking for Input in Update and setting the states
     * handling the behaviour based on states and input
     */
	void Update ( ) {

		float hInput = Input.GetAxis ( "Horizontal" );
		if ( hInput == 0 ) animator.SetFloat ( "Speed", 0 );


		switch ( stats.state ) {
		case "idle":
			if ( hInput != 0 ) {
				stats.state = "walk";
				Move ( hInput );
			} else if ( Input.GetButton ( "Duck" ) ) {
				Duck ( hInput );
			} else if ( Input.GetButtonDown ( "Shout" ) ) {
				attackScript.Shout ( );

			} else if ( Input.GetButtonDown ( "Jump" ) ) {
				Jump ( );
			}
			break;
		case "walk":
			if ( hInput == 0 ) {
				stats.state = "idle";
			} else if ( Input.GetButtonDown ( "Jump" ) ) {
				Jump ( );
			} else if ( Input.GetButton ( "Duck" ) ) {
				Duck ( hInput );
			} else {
				Move ( hInput );
			}
			break;
		case "duck":
			if ( !Input.GetButton ( "Duck" ) ) {
				Duck ( hInput );
			} else if ( hInput != 0 ) {
				stats.state = "run";
			}
			break;
		case "run":
			if ( !Input.GetButton ( "Duck" ) ) {
				Duck ( hInput );
			} else if ( Input.GetButtonDown ( "Strike" ) ) {
				attackScript.HeadBump ( );
			} else {
				Run ( );
			}
			break;
		case "jump": 
			if ( stats.isGrounded ) stats.state = "idle";
			if ( Input.GetButtonDown ( "Jump" ) ) Jump ( );
			if ( hInput != 0 ) Move ( hInput );
			break;
		default:
			stats.state = "idle";
			break;
		}

		if ( Input.GetButtonDown ( "Strike" ) && ( stats.state == "idle" || stats.state == "walk" || stats.state == "jump" ) ) {
			attackScript.Strike ( );
		}

		if ( Input.GetButtonDown ( "Interact" ) ) {
			InteractWithObject ( );  
		}
	}

	/*
     * checking animator important state-variables
     */
	void FixedUpdate ( ) {        
		stats.isGrounded = false;
		// checking if character isGrounded
		Collider2D [] colliders = Physics2D.OverlapCircleAll ( groundCheck.position, groundCheckRadius, groundLayerMask );
		for ( int i = 0; i < colliders.Length; i++ ) {
			if ( colliders [ i ].gameObject != gameObject ) stats.isGrounded = true;
		}

		if ( !stats.isGrounded ) stats.state = "jump"; 

		if ( Input.GetButtonUp ( "Duck" ) ) stats.isDucking = false;

		animator.SetBool ( "isGrounded", stats.isGrounded );
		animator.SetBool ( "isDucking", stats.isDucking );
		animator.SetFloat ( "vSpeed", rb2d.velocity.y );

	}

	/*
     * handle the movement input meaning walking and running
     */
	private void Move ( float _walkingSpeed ) {
		// The Speed animator parameter is set to the absolute value of the horizontal input.
		animator.SetFloat ( "Speed", Mathf.Abs ( _walkingSpeed ) );

		rb2d.velocity = new Vector2 ( _walkingSpeed * stats.maxWalkingSpeed, rb2d.velocity.y );

		if ( _walkingSpeed > 0 && !stats.isFacingRight || _walkingSpeed < 0 && stats.isFacingRight ) {
			Flip ( );
		}   

	}

	/*
     * handle the movement input meaning walking and running
     */
	private void Run ( ) {
		// The Speed animator parameter is set to the absolute value of the horizontal input.
		animator.SetFloat ( "Speed", Mathf.Abs ( stats.runningSpeed ) );

		if ( !stats.isFacingRight ) rb2d.velocity = new Vector2 ( -stats.runningSpeed, rb2d.velocity.y );
		else rb2d.velocity = new Vector2 ( stats.runningSpeed, rb2d.velocity.y );

	}

	/*
     * flipping the character sprite to fit the moving direction
     */
	private void Flip ( ) {
		stats.isFacingRight = !stats.isFacingRight;
		Vector3 scale = transform.localScale;
		scale.x *= -1;
		transform.localScale = scale;
	}

	/*
     * function to controll the jumping/ flying of the character
     * constraining the jump-height through adjusting the jump-force
     * sets boolean isGrounded to false
     */
	private void Jump ( ) {
		if ( stats.isGrounded && animator.GetBool ( "isGrounded" ) ) {
			//first jump
			jumpStartHeight = transform.position.y;
			stats.isGrounded = false;
			animator.SetBool ( "isGrounded", false );
		} 
		// calculating current jump-force depending on the y-position when pressed jump
		currentJumpForce = Mathf.Min ( ( 1 - ( transform.position.y - jumpStartHeight ) / stats.maxJumpHeightDifference ), 1 ) * stats.maxJumpForce;

		// adding force
		rb2d.AddForce ( new Vector2 ( 0f, currentJumpForce ) );
		AudioManager.Instance ( ).PlaySound ( "wingflap" );
	}

	/*
     * function to adjust size and offset of body collider and setting the state depending on "Duck"-Input
     * called when up or down event of Button "Duck" is noticed
     */
	private void Duck ( float hInput ) {
		CapsuleCollider2D coll = gameObject.GetComponent<CapsuleCollider2D> ( );
		if ( stats.state == "duck" || stats.state == "run" ) {
			// resetting the values to default
			coll.size = new Vector2 ( coll.size.x, bodyColliderHeight );
			coll.offset = new Vector2 ( coll.offset.x, 1f );
			if ( hInput == 0 ) stats.state = "idle";
			else stats.state = "walk";
			stats.isDucking = false;
		} else {
			// reduce size of body collider and setting state
			coll.size = new Vector2 ( coll.size.x, bodyColliderHeight - 2.5f );
			coll.offset = new Vector2 ( coll.offset.x, 0f );
			if ( hInput == 0 ) stats.state = "duck";
			else stats.state = "run";
			stats.isDucking = true;
		}
	}

	/*
     * function to respond to the character getting hit by another object
     * @ToDo reduce health, audiovisual feedback
     */
	private void GotHit ( GameObject hitObj ) {
        
	}

	/*
     * moves the character a bit against the looking direction as reaction to getting hit or running into concret
     */
	private void BounceBack ( ) {
		float xBounceBack = stats.feedbackForce;
		if ( stats.isFacingRight ) {
			xBounceBack *= -1;
		}
		transform.position = new Vector3 ( transform.position.x + xBounceBack, transform.position.y, transform.position.z );
	}

	private void playMovementSound ( string soundName ) {
		AudioManager.Instance ( ).PlaySound ( soundName );
	}

	private void InteractWithObject ( ) {
		if ( interactableObject != null ) {
			interactableObject.SendMessageUpwards ( "Interact" );
			interactableObject = null;
		}
	}


	private void OnTriggerEnter2D ( Collider2D other ) {
		if ( other.tag == "Interactable" ) {
			interactableObject = other.gameObject;
		}
	}

	private void OnTriggerExit2D ( Collider2D other ) {
		if ( other.tag == "Interactable" ) {
			interactableObject = null;
		}
	}

	/*
     * checks collision for the various states of the character
     */
	private void OnCollisionEnter2D ( Collision2D collider ) {
		if ( stats.state == "run" ) {
			stats.state = "idle";
			animator.SetBool ( "isRunning", false );
		}
		GotHit ( collider.gameObject );        
	}
}
