﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulAction : MonoBehaviour {

    private Collider2D shoutCol;
    private float countdown = 0;

    void Start ( ) {
        shoutCol = transform.GetComponent<CircleCollider2D> ( );
        shoutCol.enabled = false;
    }

    void Update ( ) {
        if ( Input.GetButtonDown ( "Shout" ) ) callOut ( );
        if ( countdown > 0 ) {
            countdown -= Time.deltaTime;
        } else if ( shoutCol.enabled == true ) {
            shoutCol.enabled = false;
        }
    }

    private void callOut ( ) { 
        shoutCol.enabled = true;
        countdown = 1f;
        AudioManager.Instance ( ).PlaySound ( "ping" );
        EffectManager.Instance ( ).showEffectAt ( "shout", transform.position );  
    
    }

    private void takeOverDemon ( GameObject demon ) {
        demon.GetComponent<DemonMovement> ( ).enabled = true;
        demon.GetComponent<PlayerAttack> ( ).enabled = true;
        CompleteCameraController.Instance ( ).followDemon ( );
        Destroy ( gameObject );
    }

    void OnTriggerEnter2D ( Collider2D col ) {
        if ( col.gameObject.tag == "Player" ) {
            takeOverDemon ( col.gameObject );
        }
    }
}
