﻿using System.Collections;
using UnityEngine;

public class PlayerStats : MonoBehaviour {

	//character
	public float weight = 100f;
	public float maxHealth = 100f;
	public float health;

	//movement
	public float maxWalkingSpeed = 10f;
	public float runningSpeed = 15f;

	//jumping
	public float maxJumpForce = 400f;
	public float maxJumpHeight;
	public float maxJumpHeightDifference;

	//attacking
	public float armorFactor = 1f;
	public float weaponFactor = 1f;
	public float feedbackForce = 4f;
	public float attackCooldown = 0.5f;
	public float smashForce = 1000f;
	public float strikeDownDamage = 100f;
	public float strikeStraightDamage = 50f;
	public float shoutDamage = 25f;
	public float smashDamage = 100f;

	//states
	public bool isAttacking = false;
	public bool isJumping = false;
	public bool isFacingRight = false;
	public bool isGrounded = false;
	public bool isDucking = false;

	public string state = "idle";

	void Awake ( ) {
		maxJumpHeightDifference = ( maxJumpForce / weight ) * 2;
		health = maxHealth;
	}

	void Update ( ) {
		if ( health <= 0 ) playerDeath ( );
	}

	private void playerDeath ( ) {
		Debug.Log ( "Player died" );
		//transform.GetComponent<DemonMovement> ( ).enabled = false;
		//gameObject.tag = "Untagged";
		health = maxHealth;
	}

	public void playerResurrect ( ) {
		Debug.Log ( "Player lives again!" );
		health = maxHealth;
		transform.GetComponent<DemonMovement> ( ).enabled = true;
		gameObject.tag = "Player";
        
	}

	public void takeDamage ( float damageAmount ) {
		health -= damageAmount;
		//Debug.Log ( "Player is taking damage of " + damageAmount );
	}

}
