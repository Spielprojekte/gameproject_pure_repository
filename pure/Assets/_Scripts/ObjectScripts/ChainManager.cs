﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainManager : MonoBehaviour {

    public bool isBroken = false;
    [SerializeField]
    private GameObject IK;

    // Use this for initialization
    void Start ( ) {
        IK = transform.Find ( "IK" ).gameObject;
    }

    public void Break ( ) {
        if ( !isBroken ) {
            AudioManager.Instance ( ).PlaySound ( "chain_break" );
            AudioManager.Instance ( ).PlaySound ( "chain_drop" );
            isBroken = true;
            IK.GetComponent<HardFollow> ( ).enabled = false;
            IK.transform.localPosition = new Vector3 ( IK.transform.localPosition.x, 0, 0 );
        }
    }
}
