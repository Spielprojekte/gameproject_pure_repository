﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : MonoBehaviour {

    public string soundName;

    public void Break ( ) {
        AudioManager.Instance ( ).PlaySound ( soundName );
        Destroy ( gameObject );
    }
}
