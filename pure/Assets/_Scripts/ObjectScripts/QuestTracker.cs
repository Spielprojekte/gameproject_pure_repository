﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestTracker : MonoBehaviour {

    public GameObject keyPrefab;
    public GameObject uiPrefab;
    public Collider2D trigger;
    public string message = "";
    public int counter = 0;

    private GameObject player;
    private GameObject goblet;
    private GameObject barrier;
    private Transform keyPos;

    private GameObject uiGo;
    private float barrierGoalPos;
    private bool isOpening = false;
    private bool hasOpend = false;
    private bool uiActive = false;


    private void Start ( ) {
        if ( !goblet ) goblet = transform.Find ( "switch" ).gameObject;
        if ( !barrier ) barrier = transform.Find ( "barrier" ).gameObject;
        barrierGoalPos = barrier.transform.position.y + 23;
        if ( !keyPos ) keyPos = transform.Find ( "keyPosition" );
    }

    private void Update ( ) {
        if ( isOpening ) {
            moveBarrier ( );
        }
    }

    public void addToCounter ( ) {
        counter++;
    }

    public void resetCounter ( ) {
        if ( !hasOpend ) {
            counter = 0;
        }
    }

    public void Interact ( ) {
        if ( counter >= 1 ) {
            Instantiate ( keyPrefab, keyPos );
            isOpening = true;
            AudioManager.Instance ( ).PlaySound ( "drawbridge" );
            trigger.enabled = false;
        }
    }

    private void moveBarrier ( ) {
        if ( barrier.transform.position.y < barrierGoalPos ) {
            barrier.transform.Translate ( Vector2.up * 3.75f * Time.deltaTime );
        } else {
            isOpening = false;
            hasOpend = true;
        }        
    }


    private void OnTriggerEnter2D ( Collider2D other ) {
        if ( other.gameObject.tag == "Player" ) {
            //GetComponent<Rigidbody2D> ( ).bodyType = RigidbodyType2D.Static;
            Vector3 newPos = transform.TransformPoint ( 0, 0, 0 );
            uiGo = Instantiate ( uiPrefab, keyPos );
            uiGo.transform.GetComponentInChildren<Text> ( ).text = message;
            uiActive = true;
        }
    }

    private void OnTriggerExit2D ( Collider2D other ) {
        if ( other.gameObject.tag == "Player" ) {
            uiActive = false;
            Destroy ( uiGo );
        }

    }

}
