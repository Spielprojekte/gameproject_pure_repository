﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItem : MonoBehaviour {

    public QuestTracker quest;
    public GameObject uiPrefab;

    public bool interactable = false;
    private bool uiActive = false;

    private GameObject uiGo;

    private void Update ( ) {
        if ( uiActive ) {
            uiGo.transform.position = transform.TransformPoint ( 0, 0, 0 );
        }
    }

    public void Interact ( ) {
        if ( interactable ) {
            quest.addToCounter ( );
            gameObject.SendMessageUpwards ( "Die" );
        }
    }

    private void OnTriggerEnter2D ( Collider2D other ) {
        if ( other.gameObject.tag == "Player" ) {
            //GetComponent<Rigidbody2D> ( ).bodyType = RigidbodyType2D.Static;
            Vector3 newPos = transform.TransformPoint ( 0, 0, 0 );
            uiGo = Instantiate ( uiPrefab, newPos, Quaternion.identity );
            uiActive = true;
        }
    }

    private void OnTriggerExit2D ( Collider2D other ) {
        if ( other.gameObject.tag == "Player" ) {
            uiActive = false;
            Destroy ( uiGo );
        }
        
    }
}
