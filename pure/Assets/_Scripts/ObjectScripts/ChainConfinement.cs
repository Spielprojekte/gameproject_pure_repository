﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainConfinement : MonoBehaviour {

    public GameObject [] chains;
    public GameObject bodyChains;
    public GameObject confinement;

    // Use this for initialization
    void Update ( ) {
        if ( chains.Length > 0 ) {
            bool allChainsBroken = true;
            foreach ( GameObject chain in chains ) {
                bool isChainBroken = chain.GetComponent<ChainManager> ( ).isBroken;
                if ( allChainsBroken && !isChainBroken ) allChainsBroken = false;
            }
            // Destroying the confinement walls
            if ( allChainsBroken ) {
                DestroyObject ( confinement );
                DestroyObject ( bodyChains );
                Destroy ( this );
            }
		
        }
    }
}
