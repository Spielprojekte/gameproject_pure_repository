﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OnEnterLoadScene : MonoBehaviour {

    public string sceneName;

    void Update ( ) {
        if ( Input.anyKey ) {
            SceneManager.LoadScene ( sceneName, LoadSceneMode.Single );
        }
    }
}
