﻿using System.Collections;
using UnityEngine;

public class AnimationController : MonoBehaviour {

    public AnimationClip clip;
    private float passedTime = 0f;

    void Update ( ) {
        passedTime += Time.deltaTime;
        if ( passedTime >= clip.length ) Destroy ( gameObject );
    }
}
