﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardFollow : MonoBehaviour {

    public GameObject target;
    public bool targetFlips;
    public Vector3 targetOffset;
    private bool targetIsFlipped = false;

    // Use this for initialization
    void Start ( ) {
    }
	
    // Update is called once per frame
    void Update ( ) {

        if ( targetFlips ) {
            if ( target.transform.localScale.x > 0 && transform.localScale.x < 0
                 || target.transform.localScale.x < 0 && transform.localScale.x > 0 ) {
                //flip
                transform.localScale = new Vector3 ( transform.localScale.x * -1, transform.localScale.y, transform.localScale.z );
                targetIsFlipped = !targetIsFlipped;
            }
        }
        if ( !targetIsFlipped ) {
            this.transform.position = target.transform.position - targetOffset;            
        } else {
            this.transform.position = target.transform.position + targetOffset;
        }
    }
}
