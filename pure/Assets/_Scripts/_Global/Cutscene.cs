﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cutscene : MonoBehaviour {

    private void PlaySound ( string soundName ) {
        AudioManager.Instance ( ).PlaySound ( soundName );
    }

    private void LoadScene ( string sceneName ) {
        SceneManager.LoadScene ( sceneName, LoadSceneMode.Single );
    }
}
