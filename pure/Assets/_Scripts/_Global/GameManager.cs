﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
    
    public PlayerStats playerStats;
    public List <GameObject> spawnLocations = new List<GameObject> ( );
    public Transform portalTransform;

    static GameManager _instance;

    [SerializeField]
    private Transform healthbar;

    void Awake ( ) {
        _instance = this;
        if ( healthbar == null ) {
            healthbar = transform.Find ( "/UI" ).Find ( "Healthbar" ).Find ( "Healthbar_filler" );
        }
        if ( playerStats == null ) {
            playerStats = transform.Find ( "/Player" ).GetComponent<PlayerStats> ( );
        }
    }

    void Update ( ) {
        updateUIForCharacterStats ( );
    }

    public static GameManager Instance ( ) {
        return _instance;
    }

    /*
     * called if a spawnLocation is activated for the first time
     * adds new active spawnLocation to a list 
     */
    public void OnNewSpawnLocationActive ( GameObject newSpawnLocation ) {
        spawnLocations.Add ( newSpawnLocation );
    }

    /*
     * iterates through all the activated spawnLocations and 
     * returns the activated spawnLocation closest to a given position
     */
    public GameObject getClostestSpawnLocationTo ( Vector3 pos ) {
        float closest = -1;
        float distance = 0;
        GameObject closestSpawnLocation = null;
        foreach ( GameObject location in spawnLocations ) {
            distance = Mathf.Abs ( Vector3.Distance ( location.transform.position, pos ) );
            if ( distance < closest || closest < 0 ) {
                closest = distance;
                closestSpawnLocation = location;
            }
        }
        return closestSpawnLocation;
    }

    public bool movePlayerToPortal ( ) {
        Transform player = transform.Find ( "/Player" );
        player.position = new Vector3 ( portalTransform.position.x, portalTransform.position.y, player.position.z );
        return true;
    }

    private void updateUIForCharacterStats ( ) {
        healthbar.localScale = new Vector3 ( playerStats.health / playerStats.maxHealth, healthbar.localScale.y, healthbar.localScale.z );
    }
}
