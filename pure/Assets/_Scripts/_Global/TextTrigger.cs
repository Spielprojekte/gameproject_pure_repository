﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextTrigger : MonoBehaviour {

    public string message = "";
    public GameObject uiText;

    public bool closeOnButton = false;
    public string closeButtonName = "";

    void OnTriggerEnter2D ( Collider2D col ) {
        //Telling game to actiavte boolean
        if ( col.gameObject.tag == "Player" ) {
            Text [] textComponents = uiText.transform.GetComponentsInChildren<Text> ( );
            for ( int i = 0; i < textComponents.Length; i++ ) {
                textComponents [ i ].text = message;
            }
            uiText.SetActive ( true );
        }
    }

    void OnTriggerExit2D ( Collider2D col ) {
        //Telling game to actiavte boolean
        if ( col.gameObject.tag == "Player" && !closeOnButton ) {
            uiText.SetActive ( false );
        }
        
    }

    void Update ( ) {
        if ( closeOnButton ) {
            if ( Input.GetButton ( closeButtonName ) ) {
                uiText.SetActive ( false );
            }
        }
    }
}