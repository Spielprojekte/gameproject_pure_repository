﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delegate : MonoBehaviour {
    [SerializeField]
    public string parentFunction = "";

    public void callParentFunction ( ) {
        gameObject.SendMessageUpwards ( parentFunction );
    }

}
