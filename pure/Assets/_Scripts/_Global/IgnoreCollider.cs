﻿using System.Collections;
using UnityEngine;

public class IgnoreCollider : MonoBehaviour {

    [SerializeField]
    private Collider2D [] colliders;

    void Start ( ) {
        for ( int i = 0; i < colliders.Length; i++ ) {
            Physics2D.IgnoreCollision ( transform.GetComponent < Collider2D> ( ), colliders [ i ], true );
        }
    }
}
