﻿using UnityEngine;
using System.Collections;

public class CompleteCameraController : MonoBehaviour {

    public bool isControlled = false;
    public bool startWithSoul;
    public GameObject vCamForSoul;
    public GameObject vCamForDemon;

    static CompleteCameraController _instance;

    private GameObject vCam;

    void Start ( ) {
        vCamForSoul = transform.Find ( "vCam soul" ).gameObject;
        vCamForDemon = transform.Find ( "vCam demon" ).gameObject;
        if ( startWithSoul ) vCam = vCamForSoul;
        else vCam = vCamForDemon;

        _instance = this;

    }

    public static CompleteCameraController Instance ( ) {
        return _instance;
    }

    void Update ( ) {
        if ( vCam.activeSelf == isControlled ) vCam.SetActive ( !vCam.activeSelf );       
    }

    /*
     * deactivates current vCam and sets current vCam to vCamForDemon
     */
    public void followDemon ( ) {
        vCam.SetActive ( false );
        vCam = vCamForDemon;
    }

    /*
     * deactivates current vCam and sets current vCam to vCamForSoul
     */
    public void followSoul ( ) {
        vCam.SetActive ( false );
        vCam = vCamForSoul;
    }

    public void useBossCam ( GameObject bossCam ) {
        vCam.SetActive ( false );
        vCam = bossCam;
    }
}