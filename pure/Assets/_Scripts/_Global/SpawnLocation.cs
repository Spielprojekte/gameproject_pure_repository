﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * script to manage the registration and activation of spawn locations
 */
public class SpawnLocation : MonoBehaviour {

    bool activated = false;

    public void spawnPlayer ( GameObject playerPrefab ) {
        Instantiate ( playerPrefab, transform.position, Quaternion.identity );
    }


    void OnTriggerEnter2D ( Collider2D col2d ) {
        if ( !activated ) {
            
            if ( col2d.tag == "Player" ) {
                // activates the spawn location functionality and lights up the torches fire

                transform.Find ( "fire" ).gameObject.SetActive ( true );
                GameManager.Instance ( ).OnNewSpawnLocationActive ( gameObject );
                AudioManager.Instance ( ).PlaySound ( "fire_lit" );

                transform.GetComponent<CircleCollider2D> ( ).enabled = false;
            }
        }
    }
}
