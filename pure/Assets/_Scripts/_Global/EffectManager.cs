﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour {

    public Transform shoutEffect;
    public Transform strikeDownEffect;
    public Transform swooshEffect;

    private Transform effect;

    private static EffectManager _instance;

    void Start ( ) {
        _instance = this;
    }

    public static EffectManager Instance ( ) {
        return _instance;
    }



    public void showEffectAt ( string effectType, Vector3 showPos ) {
        switch ( effectType ) {
        case "shout":
            effect = Instantiate ( shoutEffect, showPos, Quaternion.identity );
            break;
        case "strikeDown":
            effect = Instantiate ( strikeDownEffect, showPos, Quaternion.identity );
            break;
        case "swoosh":
            effect = Instantiate ( swooshEffect, showPos, Quaternion.identity );
            break;
        default:
            break;
        }


    }
}
