﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour {

    private Dictionary <string, UnityEvent> eventDictionary;
    private static EventManager eventMan;

    public static EventManager instance {
        get {
            if ( !eventMan ) {
                eventMan = FindObjectOfType ( typeof ( EventManager ) ) as EventManager;
                if ( !eventMan ) Debug.LogError ( "No Object of Type eventManager in the scene" );
                else eventMan.Init ( );
            }
            return eventMan;
        }
    }

    public void Init ( ) {
        if ( eventDictionary == null ) eventDictionary = new Dictionary<string, UnityEvent> ( );
    }

    public static void StartListening ( string eventName, UnityAction listener ) {
        UnityEvent thisEvent = null;
        if ( instance.eventDictionary.TryGetValue ( eventName, out thisEvent ) ) {
            thisEvent.AddListener ( listener );
        } else {
            thisEvent = new UnityEvent ( );
            thisEvent.AddListener ( listener );
            instance.eventDictionary.Add ( eventName, thisEvent );
        }
    }

    public static void StopListening ( string eventName, UnityAction listener ) {
        if ( eventMan == null ) return;
        UnityEvent thisEvent = null;
        if ( instance.eventDictionary.TryGetValue ( eventName, out thisEvent ) ) {
            thisEvent.RemoveListener ( listener );
        }
    }

    public static void TriggerEvent ( string eventName ) {
        UnityEvent thisEvent = null;
        if ( instance.eventDictionary.TryGetValue ( eventName, out thisEvent ) ) {
            thisEvent.Invoke ( );
        }
    }
}
