﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    public bool playerInLevel = false;

    void OnTriggerEnter2D ( Collider2D other ) {
        if ( other.tag == "Player" ) {
            playerInLevel = true;
            GetComponent<AudioSource> ( ).Play ( );
        }
    }

    void OnTriggerExit2D ( Collider2D other ) {
        if ( other.tag == "Player" ) {
            playerInLevel = false;
            GetComponent<AudioSource> ( ).Stop ( );
        }
    }

}
