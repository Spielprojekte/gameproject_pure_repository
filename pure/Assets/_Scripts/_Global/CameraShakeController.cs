﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeController : MonoBehaviour {

    public float waitBeforeShake = 0f;
    public float shakeDuration = 0f;
    public float shakeAmount = 0.7f;

    public Vector3 originalPos;

    public Transform camTransform;

    private CompleteCameraController camController;
    private float decreaseFactor = 1.0f;

    public static CameraShakeController _instance;



    void Start ( ) {
        _instance = this;
        camController = camTransform.parent.GetComponent<CompleteCameraController> ( );
    }

    public static CameraShakeController Instance ( ) {
        return _instance;
    }

    void Update ( ) {
        if ( shakeDuration > 0 ) {
            // check if within waiting time
            if ( waitBeforeShake > 0 ) {
                waitBeforeShake -= Time.deltaTime * decreaseFactor;
            } else {
                // shake camera
                camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
                shakeDuration -= Time.deltaTime * decreaseFactor;
            }
        } else {
            //reset values
            shakeDuration = 0f;
            // camTransform.position = originalPos;
            /* resetting the position of the camera will be done by he camera controller */
            camController.isControlled = false;
        }
    }

    public void shakeCamera ( float duration, float amount ) {
        originalPos = camTransform.localPosition;
        camController.isControlled = true;
        shakeAmount = amount;
        shakeDuration = duration;
    }

    public void shakeCamera ( float duration, float amount, float wait ) {
        originalPos = camTransform.localPosition;
        camController.isControlled = true;
        shakeAmount = amount;
        waitBeforeShake = wait;
        shakeDuration = duration;
    }
}
