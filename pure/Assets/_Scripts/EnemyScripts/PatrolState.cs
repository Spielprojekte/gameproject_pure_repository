﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState {

	private Enemy owner;
	private float patrolTimer;
	private const float patrolDuration = 10f;

	public void Enter ( Enemy enemy ) {
		// Debug.Log ( "Entering PatrolState" );
		owner = enemy;
	}

	public void Exit ( ) {
	}

	public void Execute ( ) {
		if ( owner.Target != null ) {
			owner.ChangeState ( new AttackState ( ) );
		} else {
			Patrol ( );
		}
	}

	public void TriggerEnter2D ( Collider2D other ) {
	}

	private void Patrol ( ) {
		owner.Move ( );
		owner.Anim.SetFloat ( "Speed", 1 );
		patrolTimer += Time.deltaTime;

		if ( patrolTimer >= patrolDuration ) owner.ChangeState ( new IdleState ( ) );
	}

	public void OnCollisionEnter2D ( Collision2D coll ) {
		// check for collision with wall or other enemy
		if ( coll.gameObject.layer == 9 || coll.gameObject.tag == "Enemy" ) {
			owner.Flip ( );
			if ( owner.Target != null ) {
				owner.Target = null;
			}
		}
	}
}
