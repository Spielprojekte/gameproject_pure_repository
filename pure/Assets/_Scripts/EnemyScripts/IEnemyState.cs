﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyState {

	void Execute ( );

	void Enter ( Enemy enemy );

	void Exit ( );

	void TriggerEnter2D ( Collider2D other );

	void OnCollisionEnter2D ( Collision2D coll );
}
