﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {

    public float attackValue;
    public float health;

    void Update ( ) {
        if ( health <= 0 ) enemyDeath ( );
    }

    private void enemyDeath ( ) {
        Debug.Log ( "Good Job, the enemy is dead!" );
        Destroy ( gameObject );
    }
}
