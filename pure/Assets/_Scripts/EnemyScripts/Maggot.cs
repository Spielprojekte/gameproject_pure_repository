﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Maggot : Enemy {

    public float _maxHealth = 1;

    private Rigidbody2D rb2dMarble;
    private GameObject maggot;
    private GameObject marble;
    public GameObject web;

    private CircleCollider2D marbleInteractTrigger;
    private bool isDead = false;

    public override void Start ( ) {
        maggot = transform.Find ( "maggot_body" ).gameObject;
        marble = transform.Find ( "marble" ).gameObject;
        web = transform.Find ( "web" ).gameObject;
        marbleInteractTrigger = marble.GetComponents<CircleCollider2D> ( ) [ 1 ];
        marbleInteractTrigger.enabled = false;

        rb2d = maggot.GetComponent<Rigidbody2D> ( );
        rb2dMarble = marble.GetComponent<Rigidbody2D> ( );
        Anim = maggot.GetComponent<Animator> ( );
        curHealth = _maxHealth;
        ChangeState ( new WaitState ( ) );
    }

    public void Update ( ) {
        if ( Anim.enabled == false && rb2d.velocity.y >= 0.1f && isDead ) {
            Anim.applyRootMotion = true;
            Anim.enabled = true;
            Anim.SetTrigger ( "die" );
            isDead = false;
            if ( marbleInteractTrigger && marble ) {
                marbleInteractTrigger.enabled = true;
                marble.GetComponent<QuestItem> ( ).interactable = true;
            }
        }
    }

    public override void TakeDamage ( float damage ) {
        if ( !isDead ) {
            Destroy ( web );
            // let both parts fall down
            Anim.enabled = false;
            if ( marble ) {
                marble.GetComponent<CircleCollider2D> ( ).isTrigger = false;
                rb2dMarble.gravityScale = 1;
            }
            rb2d.gravityScale = 1;
            isDead = true;
        } else {
            Anim.enabled = false;
            rb2d.AddForce ( Vector2.right );
        }
    }

    public void Die ( ) {
        Destroy ( gameObject );
    }


}
