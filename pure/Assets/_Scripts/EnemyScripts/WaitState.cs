﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitState : IEnemyState {

	private Enemy owner;
	private Transform sight;

	public void Enter ( Enemy enemy ) {
		owner = enemy;

		owner.rb2d.gravityScale = 0;
		sight = owner.transform.Find ( "Sight" );
		if ( sight ) {
			sight.Rotate ( 0, 0, 90 );
		}
	}

	public void Exit ( ) {
		if ( sight ) {
			sight.Rotate ( 0, 0, -90 );
		}
		
	}

	public void Execute ( ) {
		if ( owner.Target != null ) {
			// Debug.Log ( "Player was seen! by: " + this );
			owner.rb2d.gravityScale = 1;
			owner.transform.Rotate ( 0, 0, -180 );
			owner.ChangeState ( new IdleState ( ) );
		}
	}

	public void TriggerEnter2D ( Collider2D other ) {
		Debug.Log ( "TriggerEnter2D called in waitState" );
	}


	public void OnCollisionEnter2D ( Collision2D coll ) {
	}

		
}
