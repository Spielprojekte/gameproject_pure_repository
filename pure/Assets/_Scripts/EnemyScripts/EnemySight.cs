﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour {

	[SerializeField]
	private Enemy owner;
	private Collider2D sight;

	void Awake ( ) {
		owner = transform.parent.GetComponent<Enemy> ( );
		sight = GetComponent<BoxCollider2D> ( );
	}

	void Update ( ) {
		if ( owner.Target == null && sight.enabled == false ) {
			sight.enabled = true;

		}
	}

	// Enemy sees Player and starts following
	void OnTriggerEnter2D ( Collider2D other ) {
		if ( other.tag == "Player" ) {
			owner.Target = other.gameObject;
			sight.enabled = false;
		}
	}
		

}
