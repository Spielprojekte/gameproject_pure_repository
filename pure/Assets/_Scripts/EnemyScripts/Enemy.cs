﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour {

	public Rigidbody2D rb2d{ get; protected set; }

	public GameObject damageRange{ get; protected set; }

	public GameObject Target { get; set; }

	public Animator Anim { get; protected set; }

	public float damageAmount { get; protected set; }

	public float movementSpeed{ get; protected set; }

	public float targetedDistance{ get; protected set; }

	public float attackCloseRangeDistance{ get; protected set; }

	// variables that will be set by functions in realtime
	[SerializeField]
	protected float curHealth;
	protected bool isFacingRight;
	protected IEnemyState curState;

	// variables for groundcheck
	public LayerMask groundLayerMask;
	protected Transform groundCheck;
	protected const float groundCheckRadius = .2f;


	/*
	 * basic initialisation function
	 * needs to be called from all derived class instances
	 */
	public virtual void Start ( ) {
		rb2d = gameObject.GetComponent<Rigidbody2D> ( );
		Anim = gameObject.GetComponent<Animator> ( );
		groundCheck = transform.Find ( "GroundCheck" );
	}

	/*
     * empty Attack function to implement different attack behaviour by different deprived classes
     */
	public virtual void Attack ( ) {
		//
	}

	public virtual void Update ( ) {
		if ( curHealth <= 0 ) {
			Anim.SetTrigger ( "die" );
		} 
	}

	public virtual void Die ( ) {
		Destroy ( gameObject );
	}

	public virtual void TakeDamage ( float damage ) {
		curHealth -= damage;
		Anim.SetTrigger ( "damage" );
		//Debug.Log ( "taking damage, new healt = " + curHealth );
	}

	/*
	 * function to change facing direction
	 */
	public void Flip ( ) {
		isFacingRight = !isFacingRight;
		transform.localScale = new Vector3 ( transform.localScale.x * -1, transform.localScale.y, transform.localScale.z );
	}

	/*
	 * function to change the current state to a new state
	 * makes sure Exit and Enter of the states are called correctly
	 */
	public void ChangeState ( IEnemyState newState ) {
		if ( curState != null ) curState.Exit ( );
		curState = newState;
		curState.Enter ( this );
	}

	/*
	 * basic forward move function 
	 */
	public void Move ( ) {
		//check if not falling
		if ( rb2d.velocity.y > -0.1f ) {
			CheckForEdges ( );
			Anim.SetFloat ( "speed", 1 );
			transform.Translate ( GetDirection ( ) * ( movementSpeed * Time.deltaTime ) );
		}
	}

	/*
     * small function to calculate distance between Enemy-Instance and the Target
     * takes z-coordinates NOT into calculation only x- and y-axis
     * @return {float}
     */
	public float DistanceToTarget ( ) {
		return Mathf.Abs ( Vector2.Distance ( new Vector2 ( Target.transform.position.x, Target.transform.position.y ), new Vector2 ( transform.position.x, transform.position.y ) ) );

	}

	/*
     * function to check if Target is within tracking distance
     * if not, Target will not be followed further
     */
	public bool CheckTargetInRange ( float rangeDist ) {
		float dist = DistanceToTarget ( );
		if ( dist <= rangeDist ) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * function to handle behaviour when close to an edge of a platform
	 */
	protected void CheckForEdges ( ) {
		// checking if no floor in front of character
		Collider2D collider = Physics2D.OverlapCircle ( groundCheck.position, groundCheckRadius, groundLayerMask );
		if ( collider == null ) {
			Flip ( );	
			// keep enemy from following player down from plattform
			if ( Target != null ) {
				Target = null;
			} 
		} 

	}

	/*
	 * move assistent function to get Vector2 of the direction currently facing
	 */
	private Vector2 GetDirection ( ) {
		return isFacingRight ? Vector2.right : Vector2.left;
	}
		


	/*
     * changes facing direction to follow the target
     * used when passing by player or if player is behind the enemy instance
     */
	public void LookAtTarget ( ) {
		if ( Target != null ) {
			float xDir = Target.transform.position.x - transform.position.x;
			if ( xDir < 0 && isFacingRight || xDir > 0 && !isFacingRight ) {
				Flip ( );
			}
		} 
	}



}
