﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : IEnemyState {

	private Enemy owner;

	public void Enter ( Enemy enemy ) {
		//Debug.Log ( "Entering AttackState" );
		owner = enemy;
		owner.damageRange.SetActive ( true );
	}

	public void Exit ( ) {
		//Debug.Log ( "Exiting AttackState" );
	}

	public void Execute ( ) {
		// if lost sight of target, stop attacking
		if ( owner.Target == null ) {
			owner.ChangeState ( new IdleState ( ) );
		} else if ( owner.CheckTargetInRange ( owner.attackCloseRangeDistance ) ) {
			owner.Attack ( );
		} else {
			owner.Move ( );
		}
	}

	public void TriggerEnter2D ( Collider2D other ) {
		Debug.Log ( "TriggerEnter2D called with " + other.gameObject.name );
		if ( other.gameObject.tag == "Player" ) {
			Debug.Log ( "sending Message to take damage to: " + other.gameObject.name );
			other.gameObject.SendMessage ( "takeDamage", owner.damageAmount );
		} 
	}

	public void OnCollisionEnter2D ( Collision2D coll ) {

	}

}
