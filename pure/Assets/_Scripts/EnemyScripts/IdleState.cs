﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IEnemyState {

	private Enemy owner;
	private float idleTimer;
	private const float idleDuration = 1f;

	public void Enter ( Enemy enemy ) {
		// Debug.Log ( "Entering IdleState" );
		owner = enemy;
	}

	public void Exit ( ) {
	}

	public void Execute ( ) {
		Idle ( );
		if ( owner.Target != null ) {
			owner.ChangeState ( new PatrolState ( ) );
		}
	}

	public void OnTriggerEnter2D ( Collider2D other ) {
	}

	void Idle ( ) {
		owner.Anim.SetFloat ( "Speed", 0 );
		idleTimer += Time.deltaTime;
		if ( idleTimer >= idleDuration ) {
			owner.ChangeState ( new PatrolState ( ) );
		}
	}



	public void TriggerEnter2D ( Collider2D other ) {
		
	}

	public void OnCollisionEnter2D ( Collision2D coll ) {

	}
}
