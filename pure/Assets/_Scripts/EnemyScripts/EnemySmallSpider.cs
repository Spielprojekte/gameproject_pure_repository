﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySmallSpider : Enemy {
	
	public float _maxHealth = 100;
	public float _moveSpeed = 5;
	public float _damage = 5f;
	public float _attackRange = 4f;
	public float _followRange = 50f;
	[SerializeField]
	protected float attackCooldown = 3f;
	protected float attackTimer;
	protected bool canAttack = true;

	/*
	 * sets variables including current state based on the specified values of the derived class
	 */
	public override void Start ( ) {
		base.Start ( );
		curHealth = _maxHealth;
		movementSpeed = _moveSpeed;
		damageAmount = _damage;
		attackCloseRangeDistance = _attackRange;
		targetedDistance = _followRange;
		isFacingRight = true;
		damageRange = transform.Find ( "DamageRange" ).gameObject;
		ChangeState ( new IdleState ( ) );
	}

	/*
	 * acts depending on the current state
	 */
	public override void Update ( ) {
		base.Update ( );

		LookAtTarget ( );
		CheckIfTargetStillWithinReach ( );
		curState.Execute ( );
	
	}

	public override void Attack ( ) {
		attackTimer += Time.deltaTime;
		if ( attackTimer >= attackCooldown ) {
			canAttack = true;
			attackTimer = 0;
		}
		if ( canAttack ) {
			Anim.SetTrigger ( "attack" );
			Anim.SetFloat ( "speed", 0 );
			canAttack = false;

			Collider2D collider = Physics2D.OverlapCircle ( damageRange.transform.position, 1f, LayerMask.GetMask ( "Player" ) );
			if ( collider != null ) {
				collider.gameObject.SendMessage ( "takeDamage", damageAmount );
				
			}
		}
	}


	protected void CheckIfTargetStillWithinReach ( ) {
		if ( Target != null && !CheckTargetInRange ( targetedDistance ) ) {
			Target = null;
		}
	}

	/*public void OnTriggerEnter2D ( Collider2D other ) {
		Collider2D copy = other;
		curState.TriggerEnter2D ( copy );
		damageRange.GetComponent<BoxCollider2D> ( ).enabled = false;

		/*if ( canAttack ) {
			Debug.Log ( "OnTriggerEnter2D" );
			if ( other.gameObject.tag == "Player" ) {
				Debug.Log ( "sending Message to take damage to: " + other.gameObject.name );
				other.gameObject.SendMessage ( "takeDamage", damageAmount );
			} 
		}
		
	}

	public void OnTriggerStay2D ( Collider2D other ) {
		Collider2D copy = other;
		curState.TriggerEnter2D ( copy );
		damageRange.GetComponent<BoxCollider2D> ( ).enabled = false;
		
	}*/

}
