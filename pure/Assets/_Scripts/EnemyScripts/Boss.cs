﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : Enemy {

	public GameObject vCamBoss;
	public QuestTracker qt;
	public GameObject textBox;

	public float _maxHealth = 10f;
	public float _damage = 50f;

	[SerializeField]
	private GameObject boss;

	private int curStage = 0;
	[SerializeField]
	private float attackCooldown = 3f;
	private float attackTimer;

	private bool isAttacking = false;
	private bool wasActivated = false;

	private string text = "";

	/*
     * sets variables including current state based on the specified values of the derived class
     */
	public override void Start ( ) {
		boss = gameObject;
		rb2d = GetComponent<Rigidbody2D> ( );
		Anim = GetComponent<Animator> ( );
		curHealth = _maxHealth;
		damageAmount = _damage;
	}

	public override void Update ( ) {
		if ( wasActivated ) {
			// player steped into triggerzone at floor level
			if ( curHealth <= 0 ) {
				// boss has to die
				Anim.SetTrigger ( "die" );
				wasActivated = false;
			} else if ( curStage < 4 && Input.GetButtonDown ( "Interact" ) ) {
				// currently in dialogue with the boss
				enterNextStage ( );
			} else if ( curStage >= 4 ) {
				// in battle mode
				Attack ( );
			}
		}
	}


	public override void Die ( ) {
		boss.gameObject.SetActive ( false );
		GetComponent<AudioSource> ( ).Stop ( );
		if ( GameManager.Instance ( ).movePlayerToPortal ( ) ) {
			vCamBoss.gameObject.SetActive ( false );
			CompleteCameraController.Instance ( ).followDemon ( );
		}
	}


	public override void Attack ( ) {
		if ( !isAttacking ) {
			attackTimer += Time.deltaTime;
			if ( attackTimer >= attackCooldown * ( 1 + Random.Range ( -5, 5 ) * 0.1f ) ) {  
				chooseAttack ( );
			}
		}
	}

	private void chooseAttack ( ) {
		int rdm = Random.Range ( 0, 4 );
		switch ( rdm ) {
		case 0:
			Anim.SetTrigger ( "idle" );
			isAttacking = false;
			break;
		case 1: 
			Anim.SetTrigger ( "strikeLeft" );
			isAttacking = true;
			break;
		case 2: 
			Anim.SetTrigger ( "strikeRight" );
			isAttacking = true;
			break;
		case 3: 
			Anim.SetTrigger ( "strikeDown" );
			isAttacking = true;
			break;
		case 4: 
			Anim.SetTrigger ( "howl" );
			isAttacking = true;
			break;
        
		}
	}

	public void resetTimer ( ) {
		if ( curHealth <= 0 ) {
			Die ( );
			return;
		}
		attackTimer = 0;
		isAttacking = false;
	}

	/*
     * sends all detected collisions of collider to be handled by the current state
     */
	private void OnCollisionEnter2D ( Collision2D other ) {
		Debug.Log ( "Fenris hit something! " + other.gameObject.name );
		if ( other.gameObject.tag == "Player" ) {
			other.gameObject.SendMessage ( "takeDamage", damageAmount );
		} 
	}

	private void OnTriggerEnter2D ( Collider2D other ) {
		if ( other.tag == "Player" && curStage == 0 ) {
			wasActivated = true;
			enterNextStage ( );
		}
	}

	/*
     * 
     */
	private void useBossCam ( ) {
		vCamBoss.gameObject.SetActive ( true );
		CompleteCameraController.Instance ( ).useBossCam ( vCamBoss );
	}

	private void enterNextStage ( ) {
		curStage++;
		switch ( curStage ) {
		case 1:
			showBoss ( );
			useBossCam ( );
			text = "So you want to open the gate back to earth, heh? And you think you've proven yourself worthy after opening the barrier? Hah hah. \n\n Press LEFT MOUSE to continue";
			textBox.GetComponentInChildren<Text> ( ).text = text;
			textBox.SetActive ( true );
			break;
		case 2:
			Anim.SetTrigger ( "showEyes" );
			text = "Worthy, say you. But I smell sins, say I. Lots of sins that body of yours is carrying. Have you also sinned in my part of hell? Let me see... \n\n Press LEFT MOUSE to continue";
			textBox.GetComponentInChildren<Text> ( ).text = text;
			break;
		case 3:
			if ( qt.counter > 2 ) {
				text = "Greedy aren't you. Took more than you needed from the helpless little beeings. Oh no, not worthy you are. And I will show you what happens to sinners in hell! \n\n Press LEFT MOUSE to continue";
				textBox.GetComponentInChildren<Text> ( ).text = text;
				Anim.SetTrigger ( "showFully" );
			} else {
				text = "My, my. On the way of virtue you want to step. Well, let us see how far that path will take you. Ha ha. \n\n Press LEFT MOUSE to continue";
				textBox.GetComponentInChildren<Text> ( ).text = text;
			}
			break;
		case 4:
			if ( qt.counter > 2 ) {
				textBox.SetActive ( false );
				Anim.SetTrigger ( "idle" );
				isAttacking = false;
				wasActivated = true;
				GetComponent<AudioSource> ( ).Play ( );
			} else {
				text = "Go your way. The others won't be as lenient... \n\n Press LEFT MOUSE to continue";
				textBox.GetComponentInChildren<Text> ( ).text = text;
				Anim.SetTrigger ( "start" );
				boss.SetActive ( false );
				curStage = -1;
				wasActivated = false;
				Die ( );
			}           
			break;
		}
	}

	private void showBoss ( ) {
		transform.Find ( "face" ).gameObject.SetActive ( true );
		transform.Find ( "paw_l" ).gameObject.SetActive ( true );
		transform.Find ( "paw_r" ).gameObject.SetActive ( true );
	}

	public override void TakeDamage ( float damage ) {
		curHealth -= damage;
	}

}
