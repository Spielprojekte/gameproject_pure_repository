﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * special version of the smallSpider class
 * placed on the ceiling or a wall
 * jumps down when player is detected
 */
public class EnemyJumperSpider : EnemySmallSpider {

	/*
	 * sets variables including current state based on the specified values of the derived class
	 */
	public override void Start ( ) {
		base.Start ( );
		curHealth = _maxHealth;
		movementSpeed = _moveSpeed;
		damageAmount = _damage;
		attackCloseRangeDistance = _attackRange;
		targetedDistance = _followRange;
		isFacingRight = true;
		ChangeState ( new WaitState ( ) );
	}
		   
}
